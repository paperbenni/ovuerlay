# ovuerlay

stream overlays using a browser source and vue js


# Nuxt 3 Minimal Starter

This is my first non-hello-world web-application. It's probably shit.

Look at the [nuxt 3 documentation](https://v3.nuxtjs.org) to learn more.

## Setup

Make sure to install the dependencies:
```bash
yarn install
```

## Development Server

Start the development server on http://localhost:3000

```bash
yarn dev -o
```

## Production

Build the application for production:

```bash
npm run build
```

Locally preview production build:

```bash
npm run preview
```

Checkout the [deployment documentation](https://v3.nuxtjs.org/guide/deploy/presets) for more information.


## Ideas

### Fullscreen title card

Library of Letter animations

### Letter intro animations library

Each letter has custom
- mask for initially hiding parts of the letter
- div with rectangles that will initially be appended to the letter
- offset for the div overlaying the letter
- optional div that will not be shrunk but moved onto the letter

Animation:
- Letter moves into position from left or right (component prop)
- Pulls appended div with it
- Appended div shrinks
- Mask animates to reveal whole letter

Future plans
- optimize more stretch divs for specific letters/sizes
- Range
- Better organisation:
```js
lettername: {
    offset: 0.3,
    overlayoffset: 0.05,
    clip-path: polygon...
    overlay: true
    overlay-clip: polygon...
    stretch: {
        all: {active: true, offset: 0, height: 1}
        top: {active: true, offset: 0, height: 1}
        bottom: {active: true, offset: 0, height: 1}
        middle: {active: true, offset: 0, height: 1}
    }
}

```



## Stuff I need to learn to do this

- [ ] tailwind custom fonts
- [ ] set transform origin
- [X] html masking
- [ ] non-tailwind transforms
- [ ] nesting components
- [X] vue non-tailwind styling


## Letter progress

- [X] A
- [X] B
- [X] C
- [X] D
- [X] E
- [X] F
- [X] G
- [X] H //maybe change animation to stretching out middle part
- [X] I
- [X] J
- [X] K
- [X] L
- [X] M
- [X] N
- [X] O
- [X] P // works PERFECTLY as of now
- [X] Q
- [X] R
- [X] S
- [X] T
- [X] U
- [X] V
- [X] W
- [X] X
- [X] Y
- [X] Z
